mod general;
mod minesweeper;
mod terminal;

use general::{Direction, Position, Size};
use minesweeper::Minesweeper;
use std::{env, io};
use terminal::{BuffWrite, Buffer, Terminal};
use termion::{event::Key, input::TermRead, raw::IntoRawMode};

fn main() -> Result<(), io::Error> {
    let args: Vec<String> = env::args().collect();

    if args.len() != 2 {
        print_help();
        return Ok(());
    }

    match &args[1][..] {
        "-h" | "--help" => {
            print_help();
            Ok(())
        }
        "-t" | "--terminal" => {
            use_terminal()?;
            Ok(())
        }
        "-g" | "--graphics" => {
            use_graphics();
            Ok(())
        }
        _ => {
            print_help();
            Ok(())
        }
    }
}

fn print_help() {
    println!("Usage: minesweeper [OPTION]");
    println!("Minesweeper game made by Erenoit. Licenced under GNU GPLv3 or later.");
    println!("Available options:");
    println!("    -h  --help        Display this help message");
    println!("    -t  --terminal    Run minesweeper in terminal mode");
    println!("    -g  --graphics    Run minesweeper in graphics mode");
}

fn use_graphics() {
    println!("This option is not ready yet. Use `minesweeper -t` instead")
}

fn use_terminal() -> io::Result<()> {
    // Setup terminal
    let stdin = io::stdin();
    let stdout = io::stdout().into_raw_mode()?;
    let mut terminal = Terminal::new(stdout);
    terminal.hide_cursor()?;
    terminal.clear()?;

    let mut minesweeper = Minesweeper::new(9, 9, 10);
    minesweeper.new_game();

    let mut buf = Buffer::simple(Size {
        width: 27,
        height: 9,
    });
    buf.change_position(Position { x: 1, y: 2 });
    buf.change_border(terminal::BorderType::Normal);

    minesweeper.write_to_buffer(&mut buf);

    let mut buf2 = Buffer::simple(Size {
        width: 27,
        height: 1,
    });
    buf2.change_position(Position { x: 1, y: 0 });

    buf2.fill_w_str("Mines: ".to_string() + &minesweeper.remaining_mine_count_str());

    terminal.write(&buf)?;
    terminal.write(&buf2)?;

    for c in stdin.keys() {
        match c.expect("Error while reading key stroke") {
            Key::Char(key) => match key {
                'q' => {
                    break;
                }
                'd' => {
                    let cur_pos = minesweeper.cursor_position();
                    minesweeper.open_cell(cur_pos.x, cur_pos.y);
                    redraw(&minesweeper, &mut buf, &mut terminal)?;

                    minesweeper.check_win();
                }
                'f' => {
                    let cur_pos = minesweeper.cursor_position();
                    minesweeper.flag_cell_toggle(cur_pos.x, cur_pos.y);
                    redraw(&minesweeper, &mut buf, &mut terminal)?;

                    buf2.empty();
                    buf2.fill_w_str(
                        "Mines: ".to_string() + &minesweeper.remaining_mine_count_str(),
                    );
                    terminal.write(&buf2)?;
                }
                'k' => {
                    minesweeper.move_cursor(Direction::Up);
                    redraw(&minesweeper, &mut buf, &mut terminal)?;
                }
                'j' => {
                    minesweeper.move_cursor(Direction::Down);
                    redraw(&minesweeper, &mut buf, &mut terminal)?;
                }
                'h' => {
                    minesweeper.move_cursor(Direction::Left);
                    redraw(&minesweeper, &mut buf, &mut terminal)?;
                }
                'l' => {
                    minesweeper.move_cursor(Direction::Right);
                    redraw(&minesweeper, &mut buf, &mut terminal)?;
                }
                _ => {}
            },
            Key::Ctrl(key) => {
                if key == 'c' || key == 'z' {
                    break;
                }
            }
            Key::Up => {
                minesweeper.move_cursor(Direction::Up);
                redraw(&minesweeper, &mut buf, &mut terminal)?;
            }
            Key::Down => {
                minesweeper.move_cursor(Direction::Down);
                redraw(&minesweeper, &mut buf, &mut terminal)?;
            }
            Key::Left => {
                minesweeper.move_cursor(Direction::Left);
                redraw(&minesweeper, &mut buf, &mut terminal)?;
            }
            Key::Right => {
                minesweeper.move_cursor(Direction::Right);
                redraw(&minesweeper, &mut buf, &mut terminal)?;
            }
            _ => {}
        }

        if minesweeper.is_lost() {
            minesweeper.new_game();
            redraw(&minesweeper, &mut buf, &mut terminal)?;

            buf2.empty();
            buf2.fill_w_str("Mines: ".to_string() + &minesweeper.remaining_mine_count_str());
            terminal.write(&buf2)?;
        }
    }

    // Recover terminal
    terminal.show_cursor()?;

    Ok(())
}

fn redraw<T: io::Write>(m: &Minesweeper, b: &mut Buffer, t: &mut Terminal<T>) -> io::Result<()> {
    m.write_to_buffer(b);

    t.write(b)?;

    Ok(())
}
