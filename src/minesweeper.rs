use crate::general::{Color, Direction, Position, Size};
use crate::terminal::{BuffWrite, Cell, Style};

pub struct Minesweeper {
    game_mode: GameMode,
    game_table: Vec<Vec<GameCell>>,
    cursor: Position,
    size: Size,
    mines: Vec<Position>,
    mine_count: usize,
    flagged_count: usize,
    visible_count: usize,
}

impl Minesweeper {
    pub fn new(width: usize, height: usize, mine_count: usize) -> Self {
        let game_table = Self::generate_game_table(width, height);
        Self {
            game_mode: GameMode::Playing,
            game_table,
            cursor: Position { x: 0, y: 0 },
            size: Size { width, height },
            mines: Vec::new(),
            mine_count,
            flagged_count: 0,
            visible_count: 0,
        }
    }

    pub fn new_game(&mut self) {
        self.game_mode = GameMode::Playing;
        self.cursor = Position { x: 0, y: 0 };
        self.flagged_count = 0;
        self.visible_count = 0;
        self.create_mines();
        self.update_map();
    }

    pub fn open_cell(&mut self, x: usize, y: usize) {
        if self.game_table[y][x].flagged {
            return;
        }
        if self.game_table[y][x].visible {
            return;
        }

        self.game_table[y][x].visible = true;
        self.visible_count += 1;

        match self.game_table[y][x].cell_type {
            CellType::Mine => {
                self.lose();
                return;
            }
            CellType::Number(_) => {
                return;
            }
            CellType::Nothing => {
                let is_left_ok = x > 0;
                let is_right_ok = x < self.size.width - 1;
                let is_up_ok = y > 0;
                let is_down_ok = y < self.size.height - 1;

                if is_left_ok && is_up_ok {
                    self.open_cell(x - 1, y - 1)
                }
                if is_left_ok && is_down_ok {
                    self.open_cell(x - 1, y + 1)
                }
                if is_right_ok && is_up_ok {
                    self.open_cell(x + 1, y - 1)
                }
                if is_right_ok && is_down_ok {
                    self.open_cell(x + 1, y + 1)
                }

                if is_left_ok {
                    self.open_cell(x - 1, y)
                }
                if is_right_ok {
                    self.open_cell(x + 1, y)
                }
                if is_up_ok {
                    self.open_cell(x, y - 1)
                }
                if is_down_ok {
                    self.open_cell(x, y + 1)
                }
            }
        }
    }

    pub fn flag_cell_toggle(&mut self, x: usize, y: usize) {
        let mut cell = &mut self.game_table[y][x];
        if !cell.visible {
            cell.flagged = !cell.flagged;
            self.flagged_count = if cell.flagged {
                self.flagged_count + 1
            } else {
                self.flagged_count - 1
            };
        }
    }

    pub fn remaining_mine_count_str(&self) -> String {
        if self.mine_count >= self.flagged_count {
            return (self.mine_count - self.flagged_count).to_string();
        } else {
            return "-".to_string() + &(self.flagged_count - self.mine_count).to_string();
        }
    }

    pub fn move_cursor(&mut self, d: Direction) {
        match d {
            Direction::Up => {
                if self.cursor.y > 0 {
                    self.cursor.y -= 1;
                }
            }
            Direction::Down => {
                if self.cursor.y < self.size.height - 1 {
                    self.cursor.y += 1;
                }
            }
            Direction::Left => {
                if self.cursor.x > 0 {
                    self.cursor.x -= 1;
                }
            }
            Direction::Right => {
                if self.cursor.x < self.size.width - 1 {
                    self.cursor.x += 1;
                }
            }
        }
    }

    pub fn cursor_position(&self) -> Position {
        self.cursor
    }

    pub fn check_win(&mut self) {
        if self.size.width * self.size.height - self.mine_count == self.visible_count {
            self.win();
        }
    }

    pub fn is_lost(&self) -> bool {
        self.game_mode == GameMode::Lose
    }

    fn generate_game_table(width: usize, height: usize) -> Vec<Vec<GameCell>> {
        let mut game_table = Vec::<Vec<GameCell>>::with_capacity((width * height).into());

        for y in 0..height {
            let mut game_row = Vec::<GameCell>::with_capacity(width.into());

            for x in 0..width {
                game_row.push(GameCell::new(Position { x, y }));
            }

            game_table.push(game_row);
        }

        game_table
    }

    fn create_mines(&mut self) {
        // Clean previous game
        self.mines = Vec::with_capacity(self.mine_count);

        while self.mines.len() < self.mine_count.into() {
            let mine = Position {
                x: rand::random::<usize>() % self.size.width,
                y: rand::random::<usize>() % self.size.height,
            };

            if !self.mines.contains(&mine) {
                self.mines.push(mine);
            }
        }
    }

    fn update_map(&mut self) {
        // Clean previous game
        for y in 0..self.size.height {
            for x in 0..self.size.width {
                self.game_table[y][x].cell_type = CellType::Nothing;
                self.game_table[y][x].visible = false;
                self.game_table[y][x].flagged = false;
            }
        }

        for mine in &self.mines {
            self.game_table[mine.y][mine.x].cell_type = CellType::Mine;
        }

        for y in 0..self.size.height {
            for x in 0..self.size.width {
                if self.game_table[y][x].cell_type != CellType::Nothing {
                    continue;
                }

                let mut surroundind_mine = 0;

                let is_left_ok = x > 0;
                let is_right_ok = x < (self.size.width - 1);
                let is_up_ok = y > 0;
                let is_down_ok = y < (self.size.height - 1);

                if is_left_ok && is_up_ok {
                    surroundind_mine += if self.game_table[y - 1][x - 1].cell_type == CellType::Mine
                    {
                        1
                    } else {
                        0
                    }
                }
                if is_left_ok && is_down_ok {
                    surroundind_mine += if self.game_table[y + 1][x - 1].cell_type == CellType::Mine
                    {
                        1
                    } else {
                        0
                    }
                }
                if is_right_ok && is_up_ok {
                    surroundind_mine += if self.game_table[y - 1][x + 1].cell_type == CellType::Mine
                    {
                        1
                    } else {
                        0
                    }
                }
                if is_right_ok && is_down_ok {
                    surroundind_mine += if self.game_table[y + 1][x + 1].cell_type == CellType::Mine
                    {
                        1
                    } else {
                        0
                    }
                }

                if is_left_ok {
                    surroundind_mine += if self.game_table[y][x - 1].cell_type == CellType::Mine {
                        1
                    } else {
                        0
                    }
                }
                if is_right_ok {
                    surroundind_mine += if self.game_table[y][x + 1].cell_type == CellType::Mine {
                        1
                    } else {
                        0
                    }
                }
                if is_up_ok {
                    surroundind_mine += if self.game_table[y - 1][x].cell_type == CellType::Mine {
                        1
                    } else {
                        0
                    }
                }
                if is_down_ok {
                    surroundind_mine += if self.game_table[y + 1][x].cell_type == CellType::Mine {
                        1
                    } else {
                        0
                    }
                }

                if surroundind_mine > 0 {
                    self.game_table[y][x].cell_type = CellType::Number(surroundind_mine);
                }
            }
        }
    }

    fn win(&mut self) {
        self.game_mode = GameMode::Win;
    }

    fn lose(&mut self) {
        self.game_mode = GameMode::Lose;
    }
}

impl BuffWrite for Minesweeper {
    fn write_to_buffer(&self, buffer: &mut crate::terminal::Buffer) {
        let buf_size = buffer.get_size();

        for x in 0..buf_size.width {
            for y in 0..buf_size.height {
                if x % 3 == 1 {
                    buffer
                        .get_mut_cell(Position { x, y })
                        .replace(self.game_table[y][x / 3].to_buff_cell());

                    if self.cursor.x == x / 3 && self.cursor.y == y {
                        buffer.get_mut_cell(Position { x: x - 1, y }).character('[');
                        buffer.get_mut_cell(Position { x: x + 1, y }).character(']');
                    } else {
                        buffer.get_mut_cell(Position { x: x - 1, y }).character(' ');
                        buffer.get_mut_cell(Position { x: x + 1, y }).character(' ');
                    }
                }
            }
        }
    }
}

struct GameCell {
    cell_type: CellType,
    visible: bool,
    flagged: bool,
    position: Position,
}

impl GameCell {
    pub fn new(position: Position) -> Self {
        Self {
            cell_type: CellType::Nothing,
            visible: false,
            flagged: false,
            position,
        }
    }

    pub fn to_buff_cell(&self) -> Cell {
        Cell::new(self.get_char(), self.get_style())
    }

    fn get_char(&self) -> char {
        if self.flagged {
            return 'f';
        }
        if !self.visible {
            return ' ';
        }

        match self.cell_type {
            CellType::Nothing => return '.',
            CellType::Number(n) => match n {
                1 => return '1',
                2 => return '2',
                3 => return '3',
                4 => return '4',
                5 => return '5',
                6 => return '6',
                7 => return '7',
                8 => return '8',
                _ => return 'O', // Impossible
            },
            CellType::Mine => return '*',
        }
    }

    fn get_style(&self) -> Style {
        if self.flagged {
            return Style::default().fg(Color::Red);
        }
        if !self.visible {
            return Style::default();
        }

        match self.cell_type {
            CellType::Nothing => return Style::default(),
            CellType::Number(_n) => return Style::default().fg(Color::Cyan),
            CellType::Mine => return Style::default().fg(Color::Yellow),
        }
    }
}

#[derive(Eq, PartialEq)]
enum CellType {
    Nothing,
    Number(u8),
    Mine,
}

#[derive(Eq, PartialEq)]
enum GameMode {
    Playing,
    Win,
    Lose,
}
