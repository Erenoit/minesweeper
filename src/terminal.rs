use crate::general::{Color, Position, Size};
use std::{
    fmt,
    fmt::{Display, Formatter},
    io,
    io::Write,
};
use termion::color;

pub struct Terminal<W: Write> {
    output: W,
}

impl<W: Write> Terminal<W> {
    pub fn new(output: W) -> Self {
        Self { output }
    }

    pub fn clear(&mut self) -> io::Result<()> {
        write!(self.output, "{}", termion::clear::All)?;
        write!(self.output, "{}", termion::cursor::Goto(1, 1))?;
        self.output.flush()
    }

    pub fn hide_cursor(&mut self) -> io::Result<()> {
        write!(self.output, "{}", termion::cursor::Hide)?;
        self.output.flush()
    }

    pub fn show_cursor(&mut self) -> io::Result<()> {
        write!(self.output, "{}", termion::cursor::Show)?;
        self.output.flush()
    }

    pub fn get_cursor(&mut self) -> io::Result<(u16, u16)> {
        termion::cursor::DetectCursorPos::cursor_pos(&mut self.output).map(|(x, y)| (x - 1, y - 1))
    }

    pub fn set_cursor(&mut self, x: usize, y: usize) -> io::Result<()> {
        write!(
            self.output,
            "{}",
            termion::cursor::Goto(x as u16 + 1, y as u16 + 1)
        )?;
        self.output.flush()
    }

    pub fn write(&mut self, buffer: &Buffer) -> io::Result<()> {
        if buffer.clean_write {
            self.clear()?;
        }

        let start_pos = if buffer.border == BorderType::None {
            Position {
                x: buffer.position.x,
                y: buffer.position.y,
            }
        } else {
            Position {
                x: buffer.position.x - 1,
                y: buffer.position.y - 1,
            }
        };

        let buf_format = format!("{}", buffer);
        let buf_vec = buf_format.split("\n\r").collect::<Vec<&str>>();

        for (line_num, line_str) in buf_vec.iter().enumerate() {
            self.set_cursor(start_pos.x, start_pos.y + line_num)?;
            write!(self.output, "{}", line_str)?;
        }

        self.output.flush()?;

        Ok(())
    }
}

pub struct Buffer {
    cells: Vec<Vec<Cell>>,
    size: Size,
    position: Position,
    border: BorderType,
    clean_write: bool,
}

impl Buffer {
    pub fn new(size: Size, position: Position, border: BorderType, clean_write: bool) -> Self {
        let v1 = || {
            let mut v1: Vec<Cell> = Vec::with_capacity(size.width);
            for _ in 0..size.width {
                v1.push(Cell::default());
            }
            return v1;
        };
        let mut v2: Vec<Vec<Cell>> = Vec::with_capacity(size.width * size.height);
        for _ in 0..size.height {
            v2.push(v1());
        }
        Self {
            cells: v2,
            size,
            position,
            border,
            clean_write,
        }
    }

    pub fn simple(size: Size) -> Self {
        Self::new(size, Position { x: 0, y: 0 }, BorderType::None, false)
    }

    pub fn change_position(&mut self, new_pos: Position) {
        self.position = new_pos;
    }

    pub fn change_border(&mut self, new_border: BorderType) {
        self.border = new_border;
    }

    pub fn clean_write(&mut self, clean_write: bool) {
        self.clean_write = clean_write;
    }

    pub fn empty(&mut self) {
        for row in &mut self.cells {
            for cell in row {
                *cell = Cell::default();
            }
        }
    }

    pub fn get_mut_cell(&mut self, pos: Position) -> &mut Cell {
        &mut self.cells[pos.y][pos.x]
    }

    pub fn get_size(&self) -> Size {
        self.size
    }

    pub fn fill_w_str(&mut self, s: String) {
        for (i, c) in s.chars().enumerate() {
            self.cells[0][i].character(c);
        }
    }

    fn new_line(&self) -> String {
        return "\n\r".to_string();
    }
}

impl Display for Buffer {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        let cell_str = format!("{}", Cell::default());
        let one_cell_size = std::mem::size_of_val(&cell_str);
        let mut str = if self.border == BorderType::None {
            Vec::<String>::with_capacity(self.size.height * (self.size.width + 2) * one_cell_size)
        } else {
            Vec::<String>::with_capacity(
                (self.size.height + 2) * (self.size.width + 4) * one_cell_size,
            )
        };

        if self.border != BorderType::None {
            str.push(self.border.corner_top_left());
            str.push(self.border.side_horizontal().repeat(self.size.width));
            str.push(self.border.corner_top_right());
            str.push(self.new_line());
        }

        for y in 0..self.size.height {
            if self.border != BorderType::None {
                str.push(self.border.side_vertical());
            }

            for x in 0..self.size.width {
                str.push(format!("{}", self.cells[y][x]));
            }

            if self.border != BorderType::None {
                str.push(self.border.side_vertical());
            }
            str.push(self.new_line());
        }

        if self.border != BorderType::None {
            str.push(self.border.corner_bottom_left());
            str.push(self.border.side_horizontal().repeat(self.size.width));
            str.push(self.border.corner_bottom_right());
            str.push(self.new_line());
        }

        write!(f, "{}", str.concat())?;

        Ok(())
    }
}

#[derive(Eq, PartialEq)]
pub enum BorderType {
    None,
    Normal,
    Curved,
}

impl BorderType {
    pub fn side_horizontal(&self) -> String {
        return '─'.to_string();
    }
    pub fn side_vertical(&self) -> String {
        return '│'.to_string();
    }
    pub fn corner_top_left(&self) -> String {
        return '┌'.to_string();
    }
    pub fn corner_top_right(&self) -> String {
        return '┐'.to_string();
    }
    pub fn corner_bottom_left(&self) -> String {
        return '└'.to_string();
    }
    pub fn corner_bottom_right(&self) -> String {
        return '┘'.to_string();
    }
}

#[derive(Clone, Copy)]
pub struct Cell {
    character: char,
    style: Style,
}

impl Cell {
    pub fn new(character: char, style: Style) -> Self {
        Self { character, style }
    }

    pub fn default() -> Cell {
        Self::new(' ', Style::default())
    }

    pub fn write_colors(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self.style.foreground {
            Color::Black => write!(f, "{}", color::Fg(color::Black))?,
            Color::Red => write!(f, "{}", color::Fg(color::Red))?,
            Color::Green => write!(f, "{}", color::Fg(color::Green))?,
            Color::Yellow => write!(f, "{}", color::Fg(color::Yellow))?,
            Color::Blue => write!(f, "{}", color::Fg(color::Blue))?,
            Color::Magenta => write!(f, "{}", color::Fg(color::Magenta))?,
            Color::Cyan => write!(f, "{}", color::Fg(color::Cyan))?,
            Color::White => write!(f, "{}", color::Fg(color::White))?,
            Color::BlackLight => write!(f, "{}", color::Fg(color::LightBlack))?,
            Color::RedLight => write!(f, "{}", color::Fg(color::LightRed))?,
            Color::GreenLight => write!(f, "{}", color::Fg(color::LightGreen))?,
            Color::YellowLight => write!(f, "{}", color::Fg(color::LightYellow))?,
            Color::BlueLight => write!(f, "{}", color::Fg(color::LightBlue))?,
            Color::MagentaLight => write!(f, "{}", color::Fg(color::LightMagenta))?,
            Color::CyanLight => write!(f, "{}", color::Fg(color::LightCyan))?,
            Color::WhiteLight => write!(f, "{}", color::Fg(color::LightWhite))?,
            Color::Reset => write!(f, "{}", color::Fg(color::Reset))?,
            Color::RGB(r, g, b) => write!(f, "{}", color::Fg(color::Rgb(r, g, b)))?,
        };

        match self.style.background {
            Color::Black => write!(f, "{}", color::Bg(color::Black))?,
            Color::Red => write!(f, "{}", color::Bg(color::Red))?,
            Color::Green => write!(f, "{}", color::Bg(color::Green))?,
            Color::Yellow => write!(f, "{}", color::Bg(color::Yellow))?,
            Color::Blue => write!(f, "{}", color::Bg(color::Blue))?,
            Color::Magenta => write!(f, "{}", color::Bg(color::Magenta))?,
            Color::Cyan => write!(f, "{}", color::Bg(color::Cyan))?,
            Color::White => write!(f, "{}", color::Bg(color::White))?,
            Color::BlackLight => write!(f, "{}", color::Bg(color::LightBlack))?,
            Color::RedLight => write!(f, "{}", color::Bg(color::LightRed))?,
            Color::GreenLight => write!(f, "{}", color::Bg(color::LightGreen))?,
            Color::YellowLight => write!(f, "{}", color::Bg(color::LightYellow))?,
            Color::BlueLight => write!(f, "{}", color::Bg(color::LightBlue))?,
            Color::MagentaLight => write!(f, "{}", color::Bg(color::LightMagenta))?,
            Color::CyanLight => write!(f, "{}", color::Bg(color::LightCyan))?,
            Color::WhiteLight => write!(f, "{}", color::Bg(color::LightWhite))?,
            Color::Reset => write!(f, "{}", color::Bg(color::Reset))?,
            Color::RGB(r, g, b) => write!(f, "{}", color::Bg(color::Rgb(r, g, b)))?,
        };

        Ok(())
    }

    pub fn character(&mut self, c: char) {
        self.character = c;
    }

    pub fn style(&mut self, s: Style) {
        self.style = s;
    }

    pub fn change(&mut self, c: char, s: Style) {
        self.character(c);
        self.style(s);
    }

    pub fn replace(&mut self, c: Cell) {
        *self = c;
    }
}

impl Display for Cell {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        self.write_colors(f)?;

        write!(f, "{}", self.character)?;

        write!(f, "{}", color::Fg(color::Reset))?;
        write!(f, "{}", color::Bg(color::Reset))?;

        Ok(())
    }
}

#[derive(Clone, Copy)]
pub struct Style {
    foreground: Color,
    background: Color,
}

impl Style {
    pub fn new(foreground: Color, background: Color) -> Self {
        Self {
            foreground,
            background,
        }
    }

    pub fn default() -> Self {
        Self::new(Color::Reset, Color::Reset)
    }

    pub fn fg(mut self, color: Color) -> Self {
        self.foreground = color;
        self
    }

    pub fn bg(mut self, color: Color) -> Self {
        self.background = color;
        self
    }
}

pub trait BuffWrite {
    fn write_to_buffer(&self, buffer: &mut Buffer);
}
