# Minesweeper

My attempt to create a minesweeper game. It is not perfect but playable.

## How to Run
1. Clone this repository:
```sh
$ git clone https://gitlab.com/Erenoit/minesweeper.git
```

2. Compile:
```sh
$ cargo build --release
```

3. Run:
```sh
$ ./target/release/minesweeper
```

## How to Play
For game rules: [wikipedia](https://www.wikiwand.com/en/Minesweeper_(video_game))

### Keys
- For movement `Arrow Keys` or `Vim keys`
- To open a cell: `d`
- To flag a cell: `f`


## Dependencies
- [Rust](https://www.rust-lang.org/)
- [Unix-like Operating System](https://www.wikiwand.com/en/Unix-like)

## Thanks
- [This repo](https://github.com/tsoding/mine) inspired me to create a minesweeper game myself.
- [termion](https://crates.io/crates/termion) for great library.
- terminal.rs file is heavily inspired by [tui-rs](https://crates.io/crates/tui). `clear()`, `hide_cursor()`, `show_cursor()`, `get_cursor()`, and `set_cursor()` functions are directly copied with very small changes.
